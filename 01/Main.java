import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

/**
 * This problem was recently asked by Google.
 * <p>
 * Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
 * <p>
 * For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
 * <p>
 * Bonus: Can you do this in one pass?
 */

public class Main {
    public static void main(String[] args) {
        System.out.println(task(generateRandomArray(), 180));
    }

    public static boolean task(int[] list, int k) {
        HashSet<Integer> numbers = arrayToSet(list); // Assuming all numbers in array are unique

        for (Integer num : numbers) {
            if (k > num) {
                if (numbers.contains(k - num))
                    return true;
            }
        }
        return false;
    }

    public static HashSet<Integer> arrayToSet(int[] list) {
        System.err.println(Arrays.toString(list));
        HashSet<Integer> numbers = new HashSet<>();
        for (int num : list)
            numbers.add(num);
        return numbers;
    }

    public static int[] generateRandomArray() {
        Random random = new Random();
        int[] n = new int[25];
        for (int i = 0; i < n.length; i++) n[i] = random.nextInt(100);
        return n;
    }
}
