from threading import Thread
import time

"""
Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.
"""


def run(f, n: int):
    time.sleep(n/1000)
    f()


def task(f, n: int):
    Thread(target=run, args=(f, n)).start()


def print_something():
    print("Hello world!")


if __name__ == "__main__":
    task(print_something, 500)
    task(print_something, 200)
    task(print_something, 300)
