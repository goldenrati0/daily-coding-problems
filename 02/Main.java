import java.util.Arrays;
import java.util.Random;

/**
 * This problem was asked by Uber.
 * <p>
 * Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
 * <p>
 * For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
 * <p>
 * Follow-up: what if you can't use division?
 */

public class Main {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(task(new int[]{1, 2, 3, 4, 5, 6, 7, 8})));
    }

    public static int[] task(int[] numbers) {
        int multiplication = multiplyArray(numbers);
        int[] n = new int[numbers.length];

        for (int i = 0; i < n.length; i++) n[i] = multiplication / numbers[i];
        return n;
    }

    private static int multiplyArray(int[] numbers) {
        int n = 1;
        for (int number : numbers) n *= number;
        return n;
    }

    public static int[] generateRandomArray() {
        Random random = new Random();
        int[] n = new int[10];
        for (int i = 0; i < n.length; i++) n[i] = random.nextInt(5);
        System.err.println(Arrays.toString(n));
        return n;
    }
}
